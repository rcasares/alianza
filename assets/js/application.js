!(function($){
    var richtext = $('textarea.wysiwyg'),
    select_list  = $('select.select2'),
    selected_tab = $('a[data-toggle="tab"]'),
    module_tgl   = $('.toggle-module'),
    module_cnt   = $('#module-content');

    // init wysihtml5
    richtext.wysihtml5({ locale: "es-AR", "stylesheets": false });
    // init chosen on selects
    select_list.select2({ no_results_text: "No hubo resultados" });
    // toggle modules sidebar
    module_tgl.click(function(e){
        e.preventDefault();
        module_cnt.fadeToggle('fast');
    });
    // selected tab localStorage persistance
    selected_tab.on('shown', function (e) {
       localStorage['selectedTab']=$(e.target).attr('href');
    });

    var selectedTab = localStorage['selectedTab'];
    if (selectedTab) $('#myTab a[href="'+selectedTab+'"]').tab('show');
})(jQuery);