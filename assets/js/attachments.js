!(function($){
    var type      = $('#attachment_type_id'),
    filename      = $('#filename'),
    browse        = $('#browse_files'),
    file          = $('#path'),
    selected_file = $('#selected_file'),
    youtube       = $('#youtube');

    // transfer click event on fake input to the real file input
    browse.click(function(e){
        file.click();
        e.preventDefault();
    });

    // when the real file input value changes reflect it's value on the fake input
    file.change(function(){
        selected_file.val(file.val().split('\\')[2]);
    });

    // on window load check for default value
    if(type.val() === '2')
    {
        youtube.show(function(){
            filename.hide();
        });
    }
    else
    {
        youtube.hide(function(){
            filename.show();
        });
    }

    // toggle between file input or url
    type.change(function(){
        if(type.val() === '2')
        {
            filename.slideUp(function(){
                youtube.slideDown();
            });
        }
        else
        {
            youtube.slideUp(function(){
                filename.slideDown();
            });
        }
    });
})(jQuery);