!(function($){
    var sortable = $('.sortable');

    // sortable sidebar menu
    sortable.sortable({
        opacity: 0.7,
        helper: "clone",
        handle: ".handle",
        stop: function(event, ui) {
            var order = $(this).sortable('toArray');
            var url = base_url + ui.item.attr('data-attr-sort') + '/order';

            $.ajax({
                url: url,
                type: 'post',
                data: { order: order }
            });
       }
    });
})(jQuery);