!(function($){
    var messages     = $('#public-messages'), // messages div
        chat_users   = $('#chat_users'), // chat active users list
        msg_input    = $('#message'), // message input box
        msg_upd_ivl  = 1000, // message refresh interval in seconds
        usr_upd_ivl  = 30000; // active users list refresh interval in seconds

    // look for new messages every x seconds
    setInterval(function(){

        $.ajax({
            type: "POST",
            url: base_url + 'chat/poll',
            data: { id: id },
            success: function(data)
            {   // parse json response and append each message to the messages div
                var message = jQuery.parseJSON(data);
                $.each(message, function(index, m) {
                    messages.append("<div class=\"message\"><span class=\"label\">"+ m.created +"</span> <span class=\"label label-info\"><i class=\"icon icon-user\"></i> "+ m.sender +"</span> "+ m.message +"</div>");
                    id = m.id;
                });
                messages.scrollTop(999999);
            }
        });

    },msg_upd_ivl);

    // updates active chat users every x seconds
    setInterval(function(){

        $.ajax({
            type: "GET",
            url: base_url + 'chat/online',
            success: function(data)
            {
                chat_users.html(data);
            }
        });

    },usr_upd_ivl);

    // post user message on enter key hit
    msg_input.keypress(function(e){

        var msg = msg_input.val();
        if(e.keyCode === 13 && msg)
        {
            $.ajax({
                type: "POST",
                url: base_url + 'chat/send',
                data: { message: msg }
            });
            msg_input.val('');
        }

    });
})(jQuery);