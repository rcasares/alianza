<?php

function active($active,$item)
{
	if($active == $item) echo 'class="active"';
}

function form_error($field)
{
    if(!empty($field)) return 'error';
}

function form_help($field)
{
    if(!empty($field)) return '<span class="help-block">'.$field.'</span>';
}

function as_dropdown($obj,$key,$value)
{
    $dd = array();
    foreach($obj as $o)
    {
        $dd[$o->{$key}] = $o->{$value};
    }
    return $dd;
}

function cmp_order($a, $b) {
    return $a['order'] - $b['order'];
}