<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Migration_add_attachments extends CI_Migration {

    public function up()
    {
        $this->dbforge->add_field(array(
            'id' => array(
                'type' => 'INT',
                'unsigned' => TRUE,
                'auto_increment' => TRUE
            ),
            'name' => array(
                'type' => 'VARCHAR',
                'constraint' => '255'
            ),
            'icon' => array(
                'type' => 'VARCHAR',
                'constraint' => '255'
            ),
            'created' => array(
                'type' => 'DATETIME',
            ),
            'updated' => array(
                'type' => 'DATETIME',
                'null' => TRUE,
            ),
        ));

        $this->dbforge->add_key('id', TRUE);
        $this->dbforge->create_table('attachment_types');

        $data = array(
            array(
                'name' => 'Audio',
                'icon' => 'icon-music',
                'created' => date('Y-m-d H:i:s'),
            ),
            array(
                'name' => 'Vídeo',
                'icon' => 'icon-film',
                'created' => date('Y-m-d H:i:s'),
            ),
            array(
                'name' => 'Documento',
                'icon' => 'icon-file',
                'created' => date('Y-m-d H:i:s'),
            ),
        );

        $this->db->insert_batch('attachment_types',$data);

        $this->dbforge->add_field(array(
            'id' => array(
                'type' => 'INT',
                'unsigned' => TRUE,
                'auto_increment' => TRUE
            ),
            'activity_id' => array(
                'type' => 'INT',
                'unsigned' => TRUE
            ),
            'attachment_type_id' => array(
                'type' => 'INT',
                'unsigned' => TRUE
            ),
            'name' => array(
                'type' => 'VARCHAR',
                'constraint' => '255'
            ),
            'file' => array(
                'type' => 'VARCHAR',
                'constraint' => '255'
            ),
            'path' => array(
                'type' => 'VARCHAR',
                'constraint' => '255'
            ),
            'created' => array(
                'type' => 'DATETIME',
            ),
            'updated' => array(
                'type' => 'DATETIME',
                'null' => TRUE,
            ),
        ));

        $this->dbforge->add_key('id', TRUE);
        $this->dbforge->add_key('activity_id');
        $this->dbforge->add_key('attachment_type_id');
        $this->dbforge->create_table('attachments');
    }

    public function down()
    {
        $this->dbforge->drop_table('attachment_types');
        $this->dbforge->drop_table('attachments');
    }
}