<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Migration_add_activities extends CI_Migration {

	public function up()
	{
		$this->dbforge->add_field(array(
			'id' => array(
				'type' => 'INT',
				'unsigned' => TRUE,
				'auto_increment' => TRUE
			),
			'module_id' => array(
				'type' => 'INT',
				'unsigned' => TRUE,
			),
			'name' => array(
				'type' => 'VARCHAR',
				'constraint' => '100',
			),
			'description' => array(
				'type' => 'TEXT',
			),
            'order' => array(
                'type' => 'INT',
            ),
			'created' => array(
				'type' => 'DATETIME',
			),
			'updated' => array(
				'type' => 'DATETIME',
                'null' => TRUE,
			),
		));

		$this->dbforge->add_key('id', TRUE);
		$this->dbforge->create_table('activities');

		$data = array(
			array(
               'module_id' => 1,
               'name' => 'M1 A1',
               'description' => 'Probando 123',
               'order' => 0,
               'created' => date('Y-m-d H:i:s'),
               'updated' => date('Y-m-d H:i:s')
            ),
			array(
               'module_id' => 1,
               'name' => 'M1 A2',
               'description' => 'Probando 123',
               'order' => 0,
               'created' => date('Y-m-d H:i:s'),
               'updated' => date('Y-m-d H:i:s')
            ),
            array(
               'module_id' => 1,
               'name' => 'M1 A3',
               'description' => 'Probando 123',
               'order' => 0,
               'created' => date('Y-m-d H:i:s'),
               'updated' => date('Y-m-d H:i:s')
            ),
            array(
               'module_id' => 2,
               'name' => 'M2 A1',
               'description' => 'Probando 123',
               'order' => 0,
               'created' => date('Y-m-d H:i:s'),
               'updated' => date('Y-m-d H:i:s')
            ),
            array(
               'module_id' => 2,
               'name' => 'M2 A2',
               'description' => 'Probando 123',
               'order' => 0,
               'created' => date('Y-m-d H:i:s'),
               'updated' => date('Y-m-d H:i:s')
            ),
            array(
               'module_id' => 2,
               'name' => 'M2 A3',
               'description' => 'Probando 123',
               'order' => 0,
               'created' => date('Y-m-d H:i:s'),
               'updated' => date('Y-m-d H:i:s')
            ),
            array(
               'module_id' => 3,
               'name' => 'M3 A1',
               'description' => 'Probando 123',
               'order' => 0,
               'created' => date('Y-m-d H:i:s'),
               'updated' => date('Y-m-d H:i:s')
            ),
            array(
               'module_id' => 3,
               'name' => 'M3 A2',
               'description' => 'Probando 123',
               'order' => 0,
               'created' => date('Y-m-d H:i:s'),
               'updated' => date('Y-m-d H:i:s')
            ),
            array(
               'module_id' => 3,
               'name' => 'M3 A3',
               'description' => 'Probando 123',
               'order' => 0,
               'created' => date('Y-m-d H:i:s'),
               'updated' => date('Y-m-d H:i:s')
            )
		);

		$this->db->insert_batch('activities',$data);
	}

	public function down()
	{
		$this->dbforge->drop_table('activities');
	}
}