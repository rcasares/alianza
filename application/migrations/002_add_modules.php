<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Migration_add_modules extends CI_Migration {

	public function up()
	{
		$this->dbforge->add_field(array(
			'id' => array(
				'type' => 'INT',
				'unsigned' => TRUE,
				'auto_increment' => TRUE
			),
			'name' => array(
				'type' => 'VARCHAR',
				'constraint' => '100',
			),
			'description' => array(
				'type' => 'TEXT',
			),
            'order' => array(
                'type' => 'INT',
            ),
			'created' => array(
				'type' => 'DATETIME',
			),
			'updated' => array(
				'type' => 'DATETIME',
                'null' => TRUE,
			),
		));

		$this->dbforge->add_key('id', TRUE);
		$this->dbforge->create_table('modules');

		$data = array(
			array(
               'name'        => 'Módulo 1',
               'description' => 'Probando 123',
               'order'       => 0,
               'created'     => date('Y-m-d H:i:s'),
               'updated'     => date('Y-m-d H:i:s')
			),
			array(
               'name'        => 'Módulo 2',
               'description' => 'Probando 123',
               'order'       => 0,
               'created'     => date('Y-m-d H:i:s'),
               'updated'     => date('Y-m-d H:i:s')
			),
            array(
               'name'        => 'Módulo 3',
               'description' => 'Probando 123',
               'order'       => 0,
               'created'     => date('Y-m-d H:i:s'),
               'updated'     => date('Y-m-d H:i:s')
            )
		);

		$this->db->insert_batch('modules',$data);
	}

	public function down()
	{
		$this->dbforge->drop_table('modules');
	}
}