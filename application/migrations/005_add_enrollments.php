<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Migration_add_enrollments extends CI_Migration {

	public function up()
	{
		$this->dbforge->add_field(array(
			'id' => array(
				'type' => 'INT',
				'unsigned' => TRUE,
				'auto_increment' => TRUE
			),
			'module_id' => array(
				'type' => 'INT',
				'unsigned' => TRUE,
			),
			'user_id' => array(
				'type' => 'INT',
				'unsigned' => TRUE,
			),
			'created' => array(
				'type' => 'DATETIME',
			),
			'updated' => array(
				'type' => 'DATETIME',
                'null' => TRUE,
			),
		));

		$this->dbforge->add_key('id', TRUE);
		$this->dbforge->add_key('module_id');
		$this->dbforge->add_key('user_id');
		$this->dbforge->create_table('enrollments');

		$data = array(
			array(
               'module_id' => 1,
               'user_id' => 2,
               'created' => date('Y-m-d H:i:s'),
               'updated' => date('Y-m-d H:i:s')
            ),
			array(
               'module_id' => 2,
               'user_id' => 2,
               'created' => date('Y-m-d H:i:s'),
               'updated' => date('Y-m-d H:i:s')
            )
		);

		$this->db->insert_batch('enrollments',$data);
	}

	public function down()
	{
		$this->dbforge->drop_table('enrollments');
	}
}