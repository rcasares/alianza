<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Migration_add_messages extends CI_Migration {

    public function up()
    {
        $this->dbforge->add_field(array(
            'id' => array(
                'type' => 'INT',
                'unsigned' => TRUE,
                'auto_increment' => TRUE
            ),
            'sender_id' => array(
                'type' => 'INT',
                'unsigned' => TRUE,
            ),
            'receiver_id' => array(
                'type' => 'INT',
                'unsigned' => TRUE,
                'null' => TRUE,
            ),
            'message' => array(
                'type' => 'TEXT'
            ),
            'created' => array(
                'type' => 'DATETIME',
            ),
            'updated' => array(
                'type' => 'DATETIME',
                'null' => TRUE,
            ),
        ));

        $this->dbforge->add_key('id', TRUE);
        $this->dbforge->add_key('sender_id');
        $this->dbforge->add_key('receiver_id');
        $this->dbforge->create_table('messages');

        $data = array(
            array(
               'sender_id'   => 1,
               'receiver_id' => 2,
               'message'     => "Hola como estas?",
               'created'     => date('Y-m-d H:i:s'),
               'updated'     => date('Y-m-d H:i:s')
            ),
            array(
               'sender_id'   => 2,
               'receiver_id' => 1,
               'message'     => "Muy bien, y tu?",
               'created'     => date('Y-m-d H:i:s'),
               'updated'     => date('Y-m-d H:i:s')
            ),
            array(
               'sender_id'   => 1,
               'receiver_id' => NULL,
               'message'     => "Buen día a todos!",
               'created'     => date('Y-m-d H:i:s'),
               'updated'     => date('Y-m-d H:i:s')
            ),
            array(
               'sender_id'   => 2,
               'receiver_id' => NULL,
               'message'     => "Gracias, buen día!",
               'created'     => date('Y-m-d H:i:s'),
               'updated'     => date('Y-m-d H:i:s')
            ),
            array(
               'sender_id'   => 1,
               'receiver_id' => NULL,
               'message'     => "Como va ese francés?",
               'created'     => date('Y-m-d H:i:s'),
               'updated'     => date('Y-m-d H:i:s')
            ),
            array(
               'sender_id'   => 2,
               'receiver_id' => NULL,
               'message'     => "Cada día mejor",
               'created'     => date('Y-m-d H:i:s'),
               'updated'     => date('Y-m-d H:i:s')
            ),
            array(
               'sender_id'   => 1,
               'receiver_id' => NULL,
               'message'     => "Me alegro mucho, hoy vamos a continuar con las actividades",
               'created'     => date('Y-m-d H:i:s'),
               'updated'     => date('Y-m-d H:i:s')
            ),
            array(
               'sender_id'   => 2,
               'receiver_id' => NULL,
               'message'     => "Perfecto, hoy de que se tratará la clase?",
               'created'     => date('Y-m-d H:i:s'),
               'updated'     => date('Y-m-d H:i:s')
            ),
            array(
               'sender_id'   => 1,
               'receiver_id' => NULL,
               'message'     => "Vamos a aprender los números",
               'created'     => date('Y-m-d H:i:s'),
               'updated'     => date('Y-m-d H:i:s')
            ),
            array(
               'sender_id'   => 2,
               'receiver_id' => NULL,
               'message'     => "Genial!",
               'created'     => date('Y-m-d H:i:s'),
               'updated'     => date('Y-m-d H:i:s')
            )
        );

        $this->db->insert_batch('messages',$data);
    }

    public function down()
    {
        $this->dbforge->drop_table('messages');
    }
}