<div class="row">
    <?php if($modules):?>
    <div class="span3">
        <div class="bordered">
        <div class="accordion" id="modules">
            <?php if($admin): ?>
            <div class="accordion-group">
                <div class="accordion-heading">
                    <a class="accordion-toggle" href="<?php echo site_url("modules/create")?>">
                        <i class="icon-plus"></i> Crear nuevo módulo
                    </a><!--/accordion-toggle -->
                </div><!--/accordion-heading -->
            </div><!--/accordion-group -->
            <?php endif ?>
            <div class="sortable">
            <?php foreach($modules as $id => $module): ?>
            <div class="accordion-group" data-attr-sort="modules" id="<?php echo $id ?>">
                <div class="accordion-heading">
                    <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#module-<?php echo $id ?>">
                        <i class="icon-folder-close-alt"></i> <?php echo $module['name'] ?>
                        <?php if ($admin): ?>
                            <span class="pull-right muted handle"><i class="icon-reorder"></i></span>
                        <?php endif ?>
                    </a><!--/accordion-toggle -->
                </div><!--/accordion-heading -->
                <div id="module-<?php echo $id?>" class="accordion-body collapse <?php echo ($id == $active_module->id) ? 'in' : '' ?>">
                    <div class="accordion-inner">
                        <ul class="nav nav-tabs nav-stacked sortable">
                            <?php if ($module['activities']): ?>
                                <?php foreach($module['activities'] as $a): ?>
                            <li data-attr-sort="activities" id="<?php echo $a['id'] ?>" <?php echo active($a['id'],$active_activity->id) ?>>
                                <a href="<?php echo site_url("browse/module/$id/activity/".$a['id']."") ?>">
                                    <i class="icon-file"></i> <?php echo $a['name'];  ?>
                                    <?php if ($admin): ?>
                                        <span class="pull-right muted handle"><i class="icon-reorder"></i></span>
                                    <?php endif ?>
                                </a>
                            </li>
                                <?php endforeach?>
                            <?php endif ?>
                        </ul><!--/nav -->
                        <ul class="nav nav-tabs nav-stacked">
                            <?php if($admin): ?>
                            <li><a href="<?php echo site_url("activities/create/$id") ?>"><i class="icon-plus"></i> Crear nueva actividad</a></li>
                            <li><a href="<?php echo site_url("enrollments/module/$id/students") ?>"><i class="icon-group"></i> Estudiantes</a></li>
                            <li><a href="<?php echo site_url("modules/edit/$id") ?>"><i class="icon-edit"></i> Editar módulo</a></li>
                            <?php endif?>
                        </ul>
                    </div><!--/accordion-inner -->
                </div><!--/accordion-body -->
            </div><!--/accordion-group -->
            <?php endforeach?>
            </div>
        </div><!--/accordion -->
        </div>
    </div><!--/span3 -->
    <div class="span9">
        <div class="bordered">
            <?php if($active_module->exists()):?>
            <div class="module">
                <h1>
                    <i class="icon-folder-open-alt"></i> <?php echo $active_module->name?>
                    <span class="pull-right"><a href="#" class="btn toggle-module"><i class="icon-plus"></i></a></span>
                </h1>
                <div id="module-content">

                    <?php echo $active_module->description?>
                    <?php if($admin):?>
                    <p>
                        <div class="btn-group">
                            <a href="<?php echo site_url("enrollments/module/$active_module->id/students")?>" class="btn btn-small"><i class="icon-group"></i> Estudiantes</a>
                            <a href="<?php echo site_url("activities/create/$active_module->id")?>" class="btn btn-small"><i class="icon-plus"></i> Actividad</a>
                            <a href="<?php echo site_url("modules/edit/$active_module->id")?>" class="btn btn-small"><i class="icon-edit"></i> Editar</a>
                            <a href="<?php echo site_url("modules/delete/$active_module->id")?>" class="btn btn-small btn-danger"><i class="icon-trash"></i> Eliminar</a>
                        </div><!--/btn-group -->
                    </p>
                    <?php endif?>
                </div><!--/module-content -->
            </div><!--/bordered-module -->
            <?php else:?>
            <div class="alert alert-info">
                <p><i class="icon-reply icon-4x pull-left"></i></p>
                <p>Selecciona alguno de los módulos disponibles en el menú a tu izquierda.<br> Luego, haz click en la actividad que desees ver.</p>
            </div><!--/alert-info -->
            <?php endif?>
            <?php if($active_activity->exists()):?>
            <br>
            <div class=" activity">
                <h2><i class="icon-file-alt"></i> <?php echo $active_activity->name ?></h2>

                <ul id="myTab" class="nav nav-tabs">
                  <li class="active"><a href="#activity-description" data-toggle="tab">Descripción</a></li>
                  <li><a href="#activity-media" data-toggle="tab">Contenidos</a></li>
                </ul><!--/nav -->
                <div id="activity-content" class="tab-content">
                    <div class="tab-pane fade in active" id="activity-description">
                        <p><?php echo $active_activity->description?></p>
                        <?php if($admin):?>
                        <hr>
                        <div class="btn-group">
                                <a href="<?php echo site_url("activities/edit/$active_activity->id")?>" class="btn btn-small"><i class="icon-edit"></i> Editar</a>
                                <a href="<?php echo site_url("activities/delete/$active_activity->id")?>" class="btn btn-small btn-danger"><i class="icon-trash"></i> Eliminar</a>
                        </div><!--/btn-group -->
                        <?php endif?>
                    </div><!--/activity-description -->
                    <div class="tab-pane fade" id="activity-media">
                        <?php if ($attachments->exists()): ?>
                        <table class="table table-bordered table-striped">
                            <tr>
                                <th>Tipo</th>
                                <th>Nombre</th>
                                <th>Acciones</th>
                            </tr>
                            <?php foreach ($attachments as $att): ?>
                            <tr>
                                <td>
                                    <i class="<?php echo $att->attachment_type_icon ?>"></i> <?php echo $att->attachment_type_name ?>
                                </td>
                                <td>
                                    <?php if($att->attachment_type_id === '2'): ?>
                                        <a href="<?php echo $att->path ?>">
                                    <?php else: ?>
                                        <a href="<?php echo site_url("attachments/download/$att->id") ?>">
                                    <?php endif ?>
                                    <?php echo $att->name ?>
                                </a>
                                </td>
                                <td>
                                    <div class="btn-group">
                                        <?php if($att->attachment_type_id === '2'): ?>
                                            <a class="btn btn-mini" href="<?php echo $att->path ?>">
                                        <?php else: ?>
                                            <a class="btn btn-mini" href="<?php echo site_url("attachments/download/$att->id") ?>">
                                        <?php endif ?>
                                            <i class="icon icon-download"></i> Descargar
                                            </a>
                                        <?php if($admin):?>
                                            <a href="<?php echo site_url("attachments/delete/$att->id") ?>" class="btn btn-mini btn-danger"><i class="icon icon-trash"></i> Eliminar</a>
                                        <?php endif?>
                                    </div>
                                </td>
                            </tr>
                            <?php endforeach ?>
                        </table><!--/nav -->
                        <?php else: ?>
                        <div class="alert alert-info">
                            <i class="icon-download"></i>
                            No hay contenidos disponibles para esta actividad
                        </div><!--/alert-info -->
                        <?php endif ?>
                        <?php if($admin):?>
                            <hr>
                            <div class="btn-group">
                                <a href="<?php echo site_url("attachments/create/$active_activity->id") ?>" class="btn btn-small"><i class="icon-plus"></i> Agregar contenidos</a>
                            </div><!--/btn-group -->
                        <?php endif?>
                    </div><!--/activity-media -->
                </div><!--/activity-content -->
            </div><!--/activity -->
            <?php endif?>
        </div><!--/bordered -->
    </div><!--/span9 -->
    <?php else: ?>
    <div class="span12">
        <div class="alert alert-error">
            <p><i class="icon-warning-sign icon-4x pull-left"></i></p>
            <p>No estás inscripto en ningún módulo todavía, pide a un administrador que te inscriba.<br/>
            Puedes enviarle un mensaje directamente desde <a href="<?php echo site_url('messages') ?>">el módulo de mensajes</a>.</p>
        </div><!--/alert-info -->
    </div>
    <?php endif ?>
</div>