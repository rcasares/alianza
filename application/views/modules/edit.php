<div class="row">
    <div class="span12">
        <div class="bordered">
        <form class="form-horizontal" method="post" action="<?php echo current_url() ?>">
            <input type="hidden" name="id" value="<?php echo $module->id?>">
            <fieldset>
                <legend><i class="icon-folder-close-alt"></i> Editar módulo</legend>
                <div class="control-group <?php echo form_error($module->error->name) ?>">
                    <label class="control-label" for="name">Nombre</label>
                    <div class="controls">
                        <input id="name" name="name" class="input-block-level" type="text" value="<?php echo $module->name?>">
                        <?php echo form_help($module->error->name) ?>
                    </div>
                </div>
                <div class="control-group <?php echo form_error($module->error->description) ?>">
                    <label class="control-label" for="description">Descripción</label>
                    <div class="controls">
                        <textarea id="description" name="description" rows="10" class="input-block-level wysiwyg" type="text"><?php echo $module->description?></textarea>
                        <?php echo form_help($module->error->description) ?>
                    </div>
                </div>
            </fieldset>
            <div class="form-actions">
                <button type="submit" class="btn btn-success">Guardar módulo</button>
                <a href="<?php echo site_url("modules/delete/$module->id") ?>" class="btn btn-danger"><i class="icon-trash"></i> Eliminar</a>
                <a href="<?php echo site_url("browse/module/$module->id") ?>" class="btn">Volver</a>
            </div>
        </form>
        </div>
    </div>
</div>