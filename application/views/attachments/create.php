<div class="row">
    <div class="span12">
        <div class="bordered">
        <form class="form-horizontal" method="post" action="<?php echo site_url("attachments/create") ?>" enctype="multipart/form-data">
            <fieldset>
                <legend><i class="icon-file-alt"></i> Crear nuevo contenido</legend>
                <div class="control-group <?php echo form_error($attachment->error->module_id) ?>">
                    <label class="control-label" for="activity_id">Actividad</label>
                    <div class="controls">
                        <select id="activity_id" name="activity_id" class="select2 input-xxlarge">
                            <?php foreach($activities as $a): ?>
                            <?php $selected = ($a->id == $attachment->activity_id) ? 'selected' : '';?>
                            <option value="<?php echo $a->id?>" <?php echo $selected?>>
                                <?php echo $a->name?>
                            </option>
                            <?php endforeach?>
                        </select>
                        <?php echo form_help($attachment->error->attachment_type_id) ?>
                    </div>
                </div>
                <div class="control-group <?php echo form_error($attachment->error->module_id) ?>">
                    <label class="control-label" for="attachment_type_id">Tipo de contenido</label>
                    <div class="controls">
                        <select id="attachment_type_id" name="attachment_type_id" class="select2 input-xxlarge">
                            <?php foreach($types as $t): ?>
                            <?php $selected = ($t->id == $attachment->attachment_type_id) ? 'selected' : '';?>
                            <option value="<?php echo $t->id?>" <?php echo $selected?>>
                                <?php echo $t->name?>
                            </option>
                            <?php endforeach?>
                        </select>
                        <?php echo form_help($attachment->error->attachment_type_id) ?>
                    </div>
                </div>
                <div class="control-group <?php echo form_error($attachment->error->name) ?>">
                    <label class="control-label" for="name">Nombre</label>
                    <div class="controls">
                        <input id="name" name="name" class="input-block-level" type="text" value="<?php echo $attachment->name?>">
                        <?php echo form_help($attachment->error->name) ?>
                    </div>
                </div>
                <div id="youtube" class="control-group <?php echo form_error($attachment->error->path) ?>">
                    <label class="control-label" for="youtube">Vídeo URL</label>
                    <div class="controls">
                        <input id="url" name="url" class="input-block-level" type="text" value="<?php echo $attachment->path?>">
                        <?php echo form_help($attachment->error->path) ?>
                    </div>
                </div>
                <div id="filename" class="control-group <?php echo form_error($attachment->error->path) ?>">
                    <label class="control-label" for="path">Archivo</label>
                    <div class="controls">
                        <div class="input-append">
                            <input id="path" name="path" type="file" style="display:none;">
                            <input id="selected_file" type="text">
                            <a id="browse_files" class="btn"><i class="icon-file-alt"></i></a>
                        </div>
                        <?php echo form_help($attachment->error->path) ?>
                    </div>
                </div>
            </fieldset>
            <div class="form-actions">
                <button type="submit" class="btn btn-success">Guardar contenido</button>
                <a href="<?php echo site_url("browse") ?>" class="btn">Volver</a>
            </div>
        </form>
        </div>
    </div>
</div>