<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title>Salle Virtuelle</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">

    <!-- Le styles -->
    <link href="<?php echo base_url("/assets/css/application.min.css")?>" rel="stylesheet">

    <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="../assets/js/html5shiv.js"></script>
    <![endif]-->

    <!-- Fav and touch icons -->
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="../assets/ico/apple-touch-icon-144-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="../assets/ico/apple-touch-icon-114-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="../assets/ico/apple-touch-icon-72-precomposed.png">
    <link rel="apple-touch-icon-precomposed" href="../assets/ico/apple-touch-icon-57-precomposed.png">
    <link rel="shortcut icon" href="../assets/ico/favicon.png">
  </head>

  <body>

    <div class="navbar navbar-fixed-top">
      <div class="navbar-inner">
        <div class="container">
          <button type="button" class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="brand" href="<?php echo site_url('/')?>"><i class="icon-fire"></i> Virtuelle</a>
          <div class="nav-collapse collapse">
            <ul class="nav">
              <li <?php active($this->uri->segment(1),'') ?>><a href="<?php echo site_url('/')?>"><i class="icon-home"></i> Inicio</a></li>
              <?php if($connected): ?>
                <?php if ($admin): ?>
                    <li <?php active($this->uri->uri_string(),'users') ?>><a href="<?php echo site_url('users') ?>"><i class="icon-group"></i> Usuarios</a></li>
                <?php endif ?>
                <li <?php active($this->uri->segment(1),'browse') ?>><a href="<?php echo site_url('browse') ?>"><i class="icon-folder-close"></i> Módulos</a></li>
                <li <?php active($this->uri->segment(1),'messages') ?>><a href="<?php echo site_url('messages') ?>"><i class="icon-envelope-alt"></i> Mensajes</a></li>
                <li <?php active($this->uri->segment(1),'chat') ?>><a href="<?php echo site_url('chat') ?>"><i class="icon-comments"></i> Chat</a></li>
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="icon-user"></i> Mi perfil <b class="caret"></b></a>
                    <ul class="dropdown-menu">
                        <li <?php active($this->uri->uri_string(),'users/update_profile') ?>><a href="<?php echo site_url('users/update_profile') ?>">Actualizar mis datos</a></li>
                        <li <?php active($this->uri->uri_string(),'users/change_password') ?>><a href="<?php echo site_url('users/change_password') ?>">Cambiar contraseña</a></li>
                        <li class="divider"></li>
                        <li><a href="<?php echo site_url('logout') ?>"><i class="icon-signout"></i> Cerrar sesión</a></li>
                    </ul>
                </li>
              <?php else: ?>
                <li <?php active($this->uri->segment(1),'signup') ?>><a href="<?php echo site_url('signup') ?>"><i class="icon-user"></i> Crear una cuenta</a></li>
                <li <?php active($this->uri->segment(1),'login') ?>><a href="<?php echo site_url('login') ?>"><i class="icon-signin"></i> Iniciar sesión</a></li>
              <?php endif ?>
            </ul>
          </div><!--/.nav-collapse -->
        </div>
      </div>
    </div>

    <div class="container">
        <?php if($this->session->flashdata('alert')):?>
        <?php   extract($this->session->flashdata('alert'))?>
        <div class="alert alert-<?php echo $type?>">
            <button type="button" class="close" data-dismiss="alert">&times;</button>
        <?php   echo $msg?>
        </div>
        <?php endif?>

        <?php echo $yield ?>

        <footer>
            <p>&copy; <?php echo date('Y') ?> Todos los derechos reservados</p>
            <p>Desarrollado por <a href="http://www.itexa.com.ar/">Itexa</a></p>
        </footer>

    </div><!-- /container -->

    <!-- Le javascript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script>
        var base_url = "<?php echo base_url(); ?>";
    </script>
    <script src="<?php echo base_url("/assets/js/application.min.js")?>"></script>

  </body>
</html>