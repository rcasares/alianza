<div class="row">
    <div class="span4 offset4">
        <div class="bordered">
        <form class="form-horizontal" method="post" action="<?php echo current_url() ?>">
            <input type="hidden" name="action" value="delete">
            <fieldset>
                <legend><i class="icon-file-alt"></i> Eliminar actividad</legend>
                <br>
                <div class="alert alert-info">
                    <i class="icon-warning-sign"></i> Ud. está a punto de eliminar una actividad, esta acción eliminará todos los contenidos relacionados a ella. ¿Está seguro que desea continuar?
                </div>
            </fieldset>
            <button type="submit" class="btn btn-danger"><i class="icon-trash"></i> Si, eliminar actividad</button>
            <a href="<?php echo site_url("browse") ?>" class="btn">Cancelar</a>
        </form>
        </div>
    </div>
</div>