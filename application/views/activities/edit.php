<div class="row">
    <div class="span12">
        <div class="bordered">
        <form class="form-horizontal" method="post" action="<?php echo current_url() ?>">
            <input type="hidden" name="id" value="<?php echo $activity->id?>">
            <fieldset>
                <legend><i class="icon-file-alt"></i> Editar actividad</legend>
                <div class="control-group <?php echo form_error($activity->error->module_id) ?>">
                    <label class="control-label" for="module_id">Módulo</label>
                    <div class="controls">
                        <select id="module_id" name="module_id" class="select2 input-block-level">
                            <?php foreach($modules as $module): ?>
                            <?php $selected = ($module->id == $activity->module_id) ? 'selected' : '';?>
                            <option value="<?php echo $module->id?>" <?php echo $selected?>>
                                <?php echo $module->name?>
                            </option>
                            <?php endforeach?>
                        </select>
                        <?php echo form_help($activity->error->module_id) ?>
                    </div>
                </div>
                <div class="control-group <?php echo form_error($activity->error->name) ?>">
                    <label class="control-label" for="name">Nombre</label>
                    <div class="controls">
                        <input id="name" name="name" class="input-block-level" type="text" value="<?php echo $activity->name?>">
                        <?php echo form_help($activity->error->name) ?>
                    </div>
                </div>
                <div class="control-group <?php echo form_error($activity->error->description) ?>">
                    <label class="control-label" for="description">Descripción</label>
                    <div class="controls">
                        <textarea id="description" name="description" rows="10" class="input-block-level wysiwyg" type="text"><?php echo $activity->description?></textarea>
                        <?php echo form_help($activity->error->description) ?>
                    </div>
                </div>
            </fieldset>
            <div class="form-actions">
                <button type="submit" class="btn btn-success">Guardar actividad</button>
                <a href="<?php echo site_url("browse/module/$activity->module_id") ?>" class="btn">Volver</a>
            </div>
        </form>
        </div>
    </div>
</div>