<div class="row">
    <div class="span4 offset4">
      <div class="bordered">
        <h3>Nueva contraseña</h3>
        <hr>
        <p>Su nueva contraseña es</p>
        <p><pre><strong><?php echo $password ?></strong></pre></p>
        <p>Tome nota de su nueva contraseña y luego inicie sesión normalmente.</p>
        <a href="<?php echo site_url('login')?>" class="btn btn-success"><i class="icon-signin"></i> Iniciar sesión</a>
      </div>
    </div>
</div>