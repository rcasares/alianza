<div class="hero-unit">
	<h1>Virtuelle</h1>
	<p>Gestión de contenidos</p>
	<hr>
	<p>
		<a href="<?php echo site_url('signup')?>" class="btn btn-large btn-success"><i class="icon-user"></i> Crear una cuenta</a>
        <a href="<?php echo site_url('login')?>" class="btn btn-large"><i class="icon-signin"></i> Iniciar sesión</a>
	</p>
</div>