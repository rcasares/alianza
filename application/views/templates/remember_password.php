<h2>Recuperación de contraseña</h2>
<p>Para recuperar su contraseña haga click en el siguiente enlace:</p>
<p><a href="<?php echo site_url('welcome/check_token/'.$token)?>"><?php echo site_url('welcome/check_token/'.$token)?></a></p>
<p>Si usted no inició el proceso de recuperación de contraseña ignore este mensaje.</p>