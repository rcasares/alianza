<div class="row">
    <div class="span12">
    <div class="bordered">
        <h1><i class="icon-envelope"></i> Mensajes</h1>
        <hr>
      <ul id="myTab" class="nav nav-tabs">
        <li class="active"><a href="#received" data-toggle="tab">Recibidos</a></li>
        <li><a href="#sent" data-toggle="tab">Enviados</a></li>
        <li><a href="#new" data-toggle="tab">Nuevo</a></li>
      </ul>
      <div class="tab-content">
        <div class="tab-pane fade in active" id="received">
            <table class="table table-striped">
                <thead>
                    <tr>
                        <th></th>
                        <th>De</th>
                        <th>Mensaje</th>
                        <th>Fecha</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach($received as $r): ?>
                    <tr>
                        <td>
                            <i class="icon-envelope-alt <?php echo ($r->readed) ? 'muted' : 'icon-highlight' ?>"></i>
                        </td>
                        <td><?php echo $r->sender_name?></td>
                        <td><?php echo $r->message?></td>
                        <td><?php echo date('H:m d/m/Y',human_to_unix($r->created))?></td>
                        <td><button class="btn btn-mini <?php echo ($r->readed) ? 'disabled' : '' ?>"><i class="icon-eye-open"></i> Leído</button></td>
                    </tr>
                    <?php endforeach?>
                </tbody>
            </table>
        </div>
        <div class="tab-pane fade" id="sent">
            <table class="table striped">
                <thead>
                    <tr>
                        <th></th>
                        <th>Para</th>
                        <th>Mensaje</th>
                        <th>Fecha</th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach($sent as $s): ?>
                    <tr>
                        <td>
                            <i class="icon-envelope-alt <?php echo ($s->readed) ? 'muted' : 'icon-highlight' ?>"></i>
                        </td>
                        <td><?php echo $s->receiver_name?></td>
                        <td><?php echo $s->message?></td>
                        <td><?php echo date('H:m d/m/Y',human_to_unix($s->created))?></td>
                    </tr>
                    <?php endforeach?>
                </tbody>
            </table>
        </div>
        <div class="tab-pane fade" id="new">
            <form class="form-horizontal" method="post" action="<?php echo site_url("messages/private_messages") ?>">
                <fieldset>
                    <div class="control-group <?php // echo form_error($activity->error->module_id) ?>">
                        <label class="control-label" for="module_id">Para</label>
                        <div class="controls">
                            <select id="recipients" name="recipients[]" class="select2 input-block-level" multiple>
                                <?php foreach($users as $u): ?>
                                <option value="<?php echo $u->id?>">
                                    <?php echo $u->name. ' '.$u->lastname?>
                                </option>
                                <?php endforeach?>
                            </select>
                            <?php // echo form_help($activity->error->module_id) ?>
                        </div>
                    </div>
                    <div class="control-group <?php // echo form_error($activity->error->description) ?>">
                        <label class="control-label" for="message">Descripción</label>
                        <div class="controls">
                            <textarea id="message" name="message" rows="10" class="input-block-level wysiwyg" type="text"><?php //echo $activity->description?></textarea>
                            <?php // echo form_help($activity->error->description) ?>
                        </div>
                    </div>
                </fieldset>
                <div class="form-actions">
                    <button type="submit" class="btn btn-success">Enviar mensaje</button>
                </div>
            </form>
        </div>
      </div>
    </div>
</div>
</div>