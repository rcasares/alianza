<div class="row">
    <div class="span9">
        <div class="bordered">
            <div id="public-messages"></div>
            <hr>
            <input type="text" id="message" placeholder="Escribe un mensaje y presiona enter..." class="input-block-level">
        </div>
    </div>
    <div class="span3">
        <div class="bordered">
            <h4><i class="icon-group"></i> Usuarios conectados</h4>
            <hr>
            <div id="chat_users">
                <?php echo $online ?>
            </div>
        </div>
    </div>
</div>
<script>
    // set last message id
    var id = "<?php echo $last?>";
</script>