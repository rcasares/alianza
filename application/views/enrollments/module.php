<div class="row">
    <div class="span12">
        <div class="bordered">
            <h2><i class="icon-group"></i> Estudiantes inscriptos en <?php echo $module->name ?></h2>
            <hr>
            <?php if($enrolled_students->exists()): ?>
                <table class="table table-bordered table-striped">
                    <tr>
                        <th>Apellido</th>
                        <th>Nombre</th>
                        <th>Email</th>
                        <th>Teléfono</th>
                        <th>Acciones</th>
                    </tr>
                <?php foreach($enrolled_students as $es): ?>
                    <tr>
                        <td><?php echo $es->lastname ?></td>
                        <td><?php echo $es->name ?></td>
                        <td><?php echo $es->email ?></td>
                        <td><?php echo $es->phone ?></td>
                        <td>
                            <div class="btn-group">
                                <a href="<?php echo site_url("enrollments/remove/$es->id/$module->id") ?>" class="btn btn-danger btn-mini"><i class="icon-trash"></i> Remover</a>
                            </div>
                        </td>
                    </tr>
                <?php endforeach?>
                </table>
            <?php else: ?>
                <div class="alert alert-info">
                    <i class="icon-info-sign"></i>
                    No hay estudiantes inscriptos en este módulo.
                </div>
            <?php endif?>
            <hr>
            <form action="<?php echo current_url()?>" class="form-inline" method="post">
                <select data-placeholder="Buscar estudiantes..." class="input-xxlarge select2" name="students[]" id="students" multiple>
                    <?php foreach ($students as $s): ?>
                        <option value="<?php echo $s->id ?>"><?php echo $s->name." ".$s->lastname ?></option>
                    <?php endforeach ?>
                </select>
                <button type="submit" class="btn btn-success"><i class="icon-plus"></i> Inscribir</button>
                <a href="<?php echo site_url("browse/module/$module->id") ?>" class="btn">Volver</a>
            </form>
        </div>
    </div>
</div>