<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Enrollments controller
 *
 * Manages enrollments CRUD
 *
 * @package     Controllers
 * @author      rcasares
 */

class Enrollments extends MY_Controller
{
	public $param = array('module');

	public function __construct()
	{
		parent::__construct();
		if(!$this->admin)
        {
            $this->session->set_flashdata('alert',array(
              'type' => 'error',
              'msg'  => '<i class="icon-warning-sign"></i> No posee privilegios para acceder a esta sección'
            ));
            redirect('browse');
        }
	}

    /**
     * shows and creates new enrollments for a given module
     *
     * @author rcasares
     **/
    public function module()
    {
        // extract $module from URI
        $module = $this->uri->segment(3);
        $m = new Module($module);
        if($this->input->post())
        {   // create new enrollment for each student
            foreach($this->input->post('students') as $student)
            {   // new enrollment
                $enroll = new Enrollment;
                // instantiate user and module
                $s = new User($student);
                // save with relationships
                $enroll->save(array($s,$m));
            }
        }

        // get all enrolled students for this module
        $enrolled = new User;
        $this->data['enrolled_students'] = $enrolled
            ->where_in_related_enrollment('module_id',$m->id)
            ->get_paged();

        // get all students not in module
        $u = new User;

        // all users in module subquery
        $sub_u = new User;
        $sub_u->select('id')->where_in_related_enrollment('module_id',$m->id);
        $this->data['students'] = $u->where_not_in_subquery('id',$sub_u)
            ->where_related_group('name','Estudiante')
            ->get_iterated();

        // set module for view
        $this->data['module'] = $m;
    }

    /**
     * deletes enrollment for a given module
     *
     * @author rcasares
     **/
    public function remove($user,$module)
    {   // remove enrollment for user in module
        $e = new Enrollment;
        $e->where(array('module_id' => $module,'user_id' => $user))->get();
        $e->delete();

        // set success alert and redirect
        $this->session->set_flashdata('alert',array(
          'type' => 'success',
          'msg'  => '<i class="icon-info-sign"></i> El usuario fue removido del módulo con éxito'
        ));
        redirect("enrollments/module/$module/students");
    }
}