<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Attachments controller
 *
 * Manages attachments CRUD
 *
 * @package     Controllers
 * @author      rcasares
 */

class Attachments extends MY_Controller
{

    public function __construct()
    {
        parent::__construct();
        if(!$this->connected)
        {
            $this->session->set_flashdata('alert',array(
              'type' => 'error',
              'msg'  => '<i class="icon-warning-sign"></i> No posee privilegios para acceder a esta sección'
            ));
            redirect('browse');
        }
    }

    /**
     * creates new attachment and associates it to an activity
     *
     * @author rcasares
     **/
    public function create($activity = FALSE, $type = FALSE)
    {
        if(!$this->admin)
        {
            $this->session->set_flashdata('alert',array(
              'type' => 'error',
              'msg'  => '<i class="icon-warning-sign"></i> No posee privilegios para acceder a esta sección'
            ));
            redirect('browse');
        }

        $attachment = new Attachment;
        if ($this->input->post())
        {
            $error = FALSE;
            $attachment->from_array($this->input->post(),array(
                'activity_id',
                'attachment_type_id',
                'name'
            ));

            if($this->input->post('attachment_type_id') === '2')
            {   // use path field for storing youtube url
                $attachment->path = prep_url($this->input->post('url'));
            }
            else
            {   // upload as regular file
                $this->load->library('upload');
                if ($this->upload->do_upload('path'))
                {
                    $upload = $this->upload->data();
                    $attachment->path = $upload['file_path'];
                    $attachment->file = $upload['file_name'];
                }
                else
                {
                    $error = $this->upload->display_errors('','');
                }
            } // end if type

            if(!$error && $attachment->save())
            {
                $this->session->set_flashdata('alert',array(
                  'type' => 'success',
                  'msg'  => '<i class="icon-info-sign"></i> Se asoció el contenido exitosamente'
                ));
                redirect($this->agent->referrer());
            }
            else
            {
                if($error) $attachment->error->path = $error;
            } // end if error

        } // end if post

        // shortcuts
        if($activity) $attachment->activity_id = $activity;
        if($type) $attachment->attachment_type_id = $type;

        // get attachment types for dropdown
        $types = new Attachment_Type;
        $types->get_iterated();

        // get activities for dropdown
        $activities = new Activity;
        $activities->get_iterated();

        // set vars to view
        $this->data['types'] = $types;
        $this->data['activities'] = $activities;
        $this->data['attachment'] = $attachment;

        // set javascript for view
        $this->js = array('assets/js/attachments.js');
    }

    /**
     * downloads an attachment from the server
     *
     * @author rcasares
     **/
    public function download($id = FALSE)
    {
        $this->view = FALSE;
        $a = new Attachment($id);
        if($a->exists())
        {
            $this->load->helper('download');
            $data = file_get_contents($a->path.$a->file);
            force_download($a->file, $data);
        }
        else
        {
            $this->session->set_flashdata('alert',array(
              'type' => 'error',
              'msg'  => '<i class="icon-warning-sign"></i> El contenido a descargar no existe'
            ));
            redirect($this->agent->referrer());
        }
    }

    /**
     * deletes an attachment from the server
     *
     * @author rcasares
     **/
    public function delete($id)
    {
        if(!$this->admin)
        {
            $this->session->set_flashdata('alert',array(
              'type' => 'error',
              'msg'  => '<i class="icon-warning-sign"></i> No posee privilegios para acceder a esta sección'
            ));
            redirect('browse');
        }

        $att = new Attachment($id);

        // delete attachment from filesystem
        unlink($att->path.$att->file);

        // delete attachment from database
        $att->where('id',$id)->delete();

        // set flash message and redirect
        $this->session->set_flashdata('alert',array(
              'type' => 'success',
              'msg'  => '<i class="icon-info-sign"></i> El contenido fué eliminado.'
        ));
        redirect($this->agent->referrer());
    }
}