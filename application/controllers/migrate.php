<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Migrate controller
 *
 * Manages migration installs and rollback
 *
 * @package     Controllers
 * @author      rcasares
 */

class Migrate extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->library('migration');
	}

	public function ultima()
	{
		if ( ! $this->migration->latest())
		{
			show_error($this->migration->error_string());
		}
		else
		{
			die('Se actualizó a la ultima versión del modelo de datos');
		}
	}

	public function version($version)
	{
		if ( ! $this->migration->version($version))
		{
			die('Se instalo el modelo de datos version='.$version);
		}
		else
		{
			show_error('No es necesario migrar se esta en la version indicada');
		}
	}
}