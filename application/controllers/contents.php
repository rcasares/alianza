<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Contents controller
 *
 * Manages module and content browsing for students
 *
 * @package     Controllers
 * @author      rcasares
 */

class Contents extends MY_Controller
{
    // Used for extracting parameters from URI
    public $params = array('module','activity');

    public function __construct()
    {
        parent::__construct();
        if(!$this->connected)
        {
            $this->session->set_flashdata('alert',array(
              'type' => 'error',
              'msg'  => '<i class="icon-warning-sign"></i> Debe iniciar sesión para ver el contenido solicitado'
            ));
            redirect('login');
        }
    }

    public function browse()
    {
        // Extract parameters from URI that match $param array
        extract($this->uri->uri_to_assoc(2,$this->params));

        // Check for enrollment first
        if($module AND !$this->admin)
        {
            $enrolled = new Enrollment;
            $enrolled->where('user_id',$this->user_id)->where('module_id',$module)->get();
            if(!$enrolled->exists())
            {
                $this->session->set_flashdata('alert',array(
                  'type' => 'error',
                  'msg'  => '<i class="icon-warning-sign"></i> No tiene permisos para ver este módulo.'
                ));
                redirect('browse');
            }

        }

        // Preload modules with activities
        $m = new Module;
        if($this->admin === TRUE) // Get all activities and related modules
        {
            $m->include_related('activity')->get();
        }
        else // Get modules where user is enrolled only
        {
            $m->where_related_enrollment('user_id',$this->user_id)->include_related('activity')->get();
        }

        $this->load->helper('text');

        // Iterate over modules and create array for sidebar menu
        $modules = array();
        foreach($m as $mod)
        {
            $modules[$mod->id]['name'] = ellipsize($mod->name,20,1);
            if($mod->activity_id)
            {
                $modules[$mod->id]['activities'][] = array(
                    'id' => $mod->activity_id,
                    'name' => ellipsize($mod->activity_name,20,1),
                    'order' => $mod->activity_order,
                );
            }
            elseif(empty($modules[$mod->id]['activities'])) $modules[$mod->id]['activities'] = array();

            usort($modules[$mod->id]['activities'],"cmp_order");
        }

        // Set vars available to view
        $this->data['modules'] = $modules;
        $this->data['active_module'] = new Module($module);
        $this->data['active_activity'] = new Activity($activity);
        $this->data['attachments'] = FALSE;

        // If activity is supplied then search for attachments
        if($activity)
        {
            $attachments = new Attachment;
            $this->data['attachments'] = $attachments->where('activity_id',$activity)
                ->include_related('attachment_type')
                ->get_iterated();
        }
    }
}