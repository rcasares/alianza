<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Activities controller
 *
 * Manages activities CRUD
 *
 * @package     Controllers
 * @author      rcasares
 */

class Activities extends MY_Controller
{
    public function __construct()
    {
        parent::__construct();
        if(!$this->admin)
        {
            $this->session->set_flashdata('alert',array(
              'type' => 'error',
              'msg'  => '<i class="icon-warning-sign"></i> No posee privilegios para acceder a esta sección'
            ));
            redirect('browse');
        }
    }

    /**
     * creates a new activity for given module
     *
     * @author rcasares
     **/
    public function create($module = FALSE)
    {
        $a = new Activity;
        if($this->input->post())
        {
            $a->from_array($_POST,array('module_id','name','description'));
            $a->validate();
            $m = new Module($_POST['module_id']);
            if($a->save($m))
            {
                $this->session->set_flashdata('alert',array(
                  'type' => 'success',
                  'msg'  => '<i class="icon-warning-sign"></i> La actividad fué guardada con éxito'
                ));
                redirect("browse/module/$m->id/activity/$a->id");
            }
        }
        // For add activity shortcut
        if($module) $a->module_id = $module;
        // Vars for the view
        $this->data['activity']  = $a;
        // For select list
        $modules = new Module();
        $this->data['modules']   = $modules->get_iterated();
        $this->data['module_id'] = $module;
    }

    /**
     * edits a given activity
     *
     * @author rcasares
     **/
    public function edit($id)
    {
        $a = new Activity();
        $a->where('id',$id)->get();
        if($this->input->post())
        {
            $a->from_array($_POST,array('module_id','name','description'));
            if($a->save())
            {
                $this->session->set_flashdata('alert',array(
                  'type' => 'success',
                  'msg'  => 'La actividad fué guardada con éxito'
                ));
                $module = $_POST['module_id'];
                redirect("browse/module/$module/activity/$a->id");
            }
        }

        // Vars for the view
        $this->data['activity']  = $a;
        // For select list
        $modules = new Module();
        $this->data['modules']   = $modules->get_iterated();
    }

    /**
     * deletes an activity
     *
     * @author rcasares
     **/
    public function delete($id)
    {
        if($this->input->post())
        {
            $a = new Activity;
            $a->where('id',$id)->get()->delete();
            $this->session->set_flashdata('alert',array(
              'type' => 'success',
              'msg'  => '<i class="icon-info-sign"></i> La actividad fué eliminada con éxito'
            ));
            redirect("browse");
        }
    }

    /**
     * orders the activities in the sidebar
     *
     * @author rcasares
     **/
    public function order()
    {
        //die(var_dump($this->input->post('order')));
        foreach($this->input->post('order') as $order => $id)
        {
            $activity = new Activity($id);
            $activity->order = $order;
            $activity->save();
        }
        $this->view = FALSE;
    }
}