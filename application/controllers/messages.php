<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Messages controller
 *
 * Manages private messages and chat room
 *
 * @package     Controllers
 * @author      rcasares
 */

class Messages extends MY_Controller
{

    public function __construct()
    {
        parent::__construct();
        if(!$this->connected)
        {
            $this->session->set_flashdata('alert',array(
              'type' => 'error',
              'msg'  => '<i class="icon-warning-sign"></i> Debe iniciar sesión para ver el contenido solicitado'
            ));
            redirect('login');
        }
    }

    /**
     * private messages between users
     *
     * @author rcasares
     **/
	public function private_messages()
	{
        $this->load->helper('date');
        // if post new messsage
        if ($this->input->post('recipients'))
        {   // create a new message for each recipient
            foreach($this->input->post('recipients') as  $r)
            {
                $message = new Message;
                $message->sender_id   = $this->user_id;
                $message->receiver_id = $r;
                $message->message     = $this->input->post('message');
                $message->save();
            }
            $this->session->set_flashdata('alert',array(
              'type' => 'success',
              'msg'  => '<i class="icon-info-sign"></i> El mensaje fue enviado'
            ));
            redirect('messages');
        }

        // received messages
        $received = new Message;
        $received->where('receiver_id',$this->user_id)->include_related('sender','name')->get_iterated();
        $this->data['received'] = $received;

        // sent messages
        $sent = new Message;
        $sent->where('sender_id',$this->user_id)->where('receiver_id !=',0)->include_related('receiver','name')->get_iterated();
        $this->data['sent'] = $sent;

        // user list for new message
        $users = new User;
        $this->data['users'] = $users->where('id !=',$this->user_id)->get_iterated();
	}

    /**
     * chat room
     *
     * @author rcasares
     **/
    public function public_messages()
    {
        $this->load->helper('date');
        $last = new Message;
        $last->select_max('id')->get();

        $this->data['last']    = $last->id;
        $this->data['online']  = $this->online();
        $this->js = array('assets/js/chat.js');
    }

    /**
     * get online users for chat
     *
     * @author ricardocasares
     **/
    public function online()
    {
        $user = new Online();
        $user->is_online($this->user_id);
        $online = new Online;
        $online->active();
        $users = array();
        foreach ($online as $o) {
            $users[] = '<a href="#"><i class="icon icon-user"></i> '.$o->user_name.' '.$o->user_lastname.'</a>';
        }

        // load helper to generate <ul>
        $this->load->helper('html');
        $attributes = array('class' => 'nav nav-tabs nav-stacked');

        if($this->input->is_ajax_request())
        {
            die(ul($users,$attributes));
        }

        return ul($users,$attributes);
    }

    /**
     * ajax send message to chat
     *
     * @author rcasares
     **/
    public function send()
    {
        $this->view = FALSE;
        if($this->input->post('message'))
        {
            $m              = new Message;
            // TODO: remove html tags from messages
            $m->message     = $this->input->post('message');
            $m->sender_id   = $this->user_id;
            $m->receiver_id = 0;
            $m->save();
        }
    }

    /**
     * ajax poll for chat messages
     *
     * @author rcasares
     **/
    public function poll()
    {
        $this->view = FALSE;
        // initial id
        $id = $this->input->post('id');
        // new message
        $message = new Message;
        $message->where('id >',$id)
            ->where('receiver_id',0)
            ->include_related('sender',array('name','lastname'))
            ->order_by('created','asc')
            ->get_iterated();

        $this->load->helper('date');

        $json = array();
        foreach ($message as $m) {
            $json[] = array(
                'id' => $m->id,
                'sender' => $m->sender_name.' '.$m->sender_lastname,
                'message' => $m->message,
                'created' => date('H:i',human_to_unix($m->created))
            );
        }

        die(json_encode($json));
    }
}