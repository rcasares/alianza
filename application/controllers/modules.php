<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Modules controller
 *
 * Manages modules CRUD
 *
 * @package     Controllers
 * @author      rcasares
 */

class Modules extends MY_Controller
{
	public function __construct()
	{
		parent::__construct();
		if(!$this->admin)
        {
            $this->session->set_flashdata('alert',array(
              'type' => 'error',
              'msg'  => '<i class="icon-warning-sign"></i> No posee privilegios para acceder a esta sección'
            ));
            redirect('browse');
        }
	}

    /**
     * creates a new module
     *
     * @author rcasares
     **/
    public function create()
    {
        $m = new Module;
        if($this->input->post())
        {
            $m->from_array($_POST,array('name','description'));
            if($m->save())
            {
                $this->session->set_flashdata('alert',array(
                  'type' => 'success',
                  'msg'  => '<i class="icon-info-sign"></i> El módulo fué guardado con éxito'
                ));
                redirect("browse/module/$m->id");
            }
        }

        $this->data['module']  = $m;
    }

    /**
     * edits a given module
     *
     * @author rcasares
     **/
    public function edit($id)
    {
        $m = new Module;
        $m->where('id',$id)->get();
        if($this->input->post())
        {
            $m->from_array($_POST,array('name','description'));
            if($m->save())
            {
                $this->session->set_flashdata('alert',array(
                  'type' => 'success',
                  'msg'  => '<i class="icon-info-sign"></i> El módulo fué guardado con éxito'
                ));
                redirect("browse/module/$m->id");
            }
        }

        // Vars for the view
        $this->data['module']  = $m;
    }

    /**
     * deletes a given module
     *
     * @author rcasares
     **/
    public function delete($id)
    {
        if($this->input->post())
        {
            $m = new Module;
            $m->where('id',$id)->get()->delete();
            $this->session->set_flashdata('alert',array(
              'type' => 'success',
              'msg'  => '<i class="icon-info-sign"></i> El módulo fué eliminado con éxito'
            ));
            redirect("browse");
        }
    }

    /**
     * orders the module in the sidebar
     *
     * @author rcasares
     **/
    public function order()
    {
        foreach($this->input->post('order') as $order => $id)
        {
            $module = new Module($id);
            $module->order = $order;
            $module->save();
        }
        $this->view = FALSE;
    }
}