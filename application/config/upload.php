<?php
// Ruta de la carpeta de subidas
$config['upload_path'] = './attachments/';
// Extensiones permitidas
$config['allowed_types'] = 'gif|jpg|png|ppt|ppx|pdf|doc|docx|xls|wav|mp3';
// Tamaño máximo de archivo en kilobytes
$config['max_size'] = '10000';