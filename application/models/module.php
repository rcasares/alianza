<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Module model
 *
 * @package     Models
 * @author      rcasares
 */

class Module extends DataMapper {

	// Insert related models that Module can have more than one of.
	var $has_many = array('activity','enrollment');

    var $default_order_by = array('order');

    var $validation = array(
        'name' => array(
            'label' => 'nombre',
            'rules' => array('required')
        ),
        'description' => array(
            'label' => 'descripción',
            'rules' => array('required')
        )
    );

	// --------------------------------------------------------------------
	// Default Ordering
	//   Uncomment this to always sort by 'name', then by
	//   id descending (unless overridden)
	// --------------------------------------------------------------------

	// var $default_order_by = array('name', 'id' => 'desc');

	// --------------------------------------------------------------------

	/**
	 * Constructor: calls parent constructor
	 */
    function __construct($id = NULL)
	{
		parent::__construct($id);
    }

    function for_user($id)
    {
    	return $this->where_related_user('id',$id)->get_iterated();
    }

}

/* End of file module.php */
/* Location: ./application/models/module.php */