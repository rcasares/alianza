<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Online model
 *
 * @package     Models
 * @author      rcasares
 */

class Online extends DataMapper {

    // override table name
    var $table = 'online_users';

    // insert related models that Module can have more than one of.
    var $has_one = array('user');

    /**
     * constructor: calls parent constructor
     */
    function __construct($id = NULL)
    {
        parent::__construct($id);
    }

    /**
     * registers user as active and calls purge method
     *
     * @author rcasares
     **/
    function is_online($user_id)
    {
        $expires = time();
        $timeout = $expires - 60;
        $online = new Online;
        $online->where('user_id',$user_id)->limit(1)->get();
        if($online->exists())
        {
            $online->expiration = $expires;
            $online->save();
            $this->purge($timeout);
            return TRUE;
        }
        else
        {
            $online = new Online;
            $online->user_id = $user_id;
            $online->expiration = $expires;
            $online->save();
            $this->purge($timeout);
            return TRUE;
        }
        return FALSE;
    }

    /**
     * removes inactive users from active users list
     *
     * @author rcasares
     **/
    function purge($timeout)
    {
        $expired = new Online;
        $expired->where('expiration <',$timeout)
            ->get()
            ->delete_all();
    }

    /**
     * returns active users
     *
     * @author rcasares
     **/
    function active()
    {
        return $this->include_related('user',array('name','lastname'))->get_iterated();
    }

}

/* End of file module.php */
/* Location: ./application/models/module.php */