<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Group model
 *
 * @package     Models
 * @author      rcasares
 */

class Group extends DataMapper {

	// Insert related models that Group can have more than one of.
	var $has_many = array('user');

    var $validation = array(
        array( // accessed via $this->confirm_email
        'field' => 'name',
        'label' => 'Confirm Email Address',
        'rules' => array('required')
    )
    );

	// --------------------------------------------------------------------
	// Default Ordering
	//   Uncomment this to always sort by 'name', then by
	//   id descending (unless overridden)
	// --------------------------------------------------------------------

	// var $default_order_by = array('name', 'id' => 'desc');

	// --------------------------------------------------------------------

	/**
	 * Constructor: calls parent constructor
	 */
    function __construct($id = NULL)
	{
		parent::__construct($id);
    }

}

/* End of file group.php */
/* Location: ./application/models/group.php */