<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Enrollment model
 *
 * @package     Models
 * @author      rcasares
 */

class Enrollment extends DataMapper {

	// Insert related models that Enrollment can have more than one of.
	var $has_one = array('module','user');

	// --------------------------------------------------------------------
	// Default Ordering
	//   Uncomment this to always sort by 'name', then by
	//   id descending (unless overridden)
	// --------------------------------------------------------------------

	// var $default_order_by = array('name', 'id' => 'desc');

	// --------------------------------------------------------------------

	/**
	 * Constructor: calls parent constructor
	 */
    function __construct($id = NULL)
	{
		parent::__construct($id);
    }

}

/* End of file enrollment.php */
/* Location: ./application/models/enrollment.php */