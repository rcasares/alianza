<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Message model
 *
 * @package     Models
 * @author      rcasares
 */

class Message extends DataMapper {

    // Insert related models that Message can have just one of.
    var $has_one = array(
        'sender' => array(
            'class' => 'user',
            'other_field' => 'sent_message'
        ),
        'receiver' => array(
            'class' => 'user',
            'other_field' => 'received_message'
        )
    );

    var $validation = array(
       'message' => array(
            'label' => 'mensaje',
            'rules' => array('xss_clean','required')
        )
    );

    // --------------------------------------------------------------------
    // Default Ordering
    //   Uncomment this to always sort by 'name', then by
    //   id descending (unless overridden)
    // --------------------------------------------------------------------

    var $default_order_by = array('created' => 'desc');

    // --------------------------------------------------------------------

    /**
     * Constructor: calls parent constructor
     */
    function __construct($id = NULL)
    {
        parent::__construct($id);
    }


}

/* End of file message.php */
/* Location: ./application/models/message.php */