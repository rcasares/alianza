<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Activity model
 *
 * @package     Models
 * @author		rcasares
 */

class Activity extends DataMapper {

	// Insert related models that Activity can have more than one of.
    var $has_one  = array('module');
    var $has_many = array('attachment');

    var $default_order_by = array('order','id');

    var $validation = array(
        'module_id' => array(
            'label' => 'módulo',
            'rules' => array('required')
        ),
        'name' => array(
            'label' => 'nombre',
            'rules' => array('required')
        ),
        'description' => array(
            'label' => 'descripción',
            'rules' => array('required')
        )
    );

	// --------------------------------------------------------------------
	// Default Ordering
	//   Uncomment this to always sort by 'name', then by
	//   id descending (unless overridden)
	// --------------------------------------------------------------------

	// var $default_order_by = array('name', 'id' => 'desc');

	// --------------------------------------------------------------------

	/**
	 * Constructor: calls parent constructor
	 */
    function __construct($id = NULL)
	{
		parent::__construct($id);
    }

}

/* End of file activity.php */
/* Location: ./application/models/activity.php */