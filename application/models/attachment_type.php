<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Attachment model
 *
 * @package     Models
 * @author      rcasares
 */

class Attachment_Type extends DataMapper {

    // Insert related models that Attachment can have more than one of.
    var $has_many = array('attachment');

    // var $validation = array(
    //     array( // accessed via $this->confirm_email
    //     'field' => 'name',
    //     'label' => 'Confirm Email Address',
    //     'rules' => array('required')
    // )
    // );

    // --------------------------------------------------------------------
    // Default Ordering
    //   Uncomment this to always sort by 'name', then by
    //   id descending (unless overridden)
    // --------------------------------------------------------------------

    // var $default_order_by = array('name', 'id' => 'desc');

    // --------------------------------------------------------------------

    /**
     * Constructor: calls parent constructor
     */
    function __construct($id = NULL)
    {
        parent::__construct($id);
    }

}

/* End of file attachment.php */
/* Location: ./application/models/attachment.php */